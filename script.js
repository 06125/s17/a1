function printNumber(num) {
	console.log(`The number you provided is ${num}.`);
	for (let x = num; x >= 50; x--) {
		if (x <= 50) { //If the number is 50 or less, the loop will break immediately.
			console.log(`The current value is at ${x}. Terminating the loop.`)
			break;
		} else if (x % 5 !== 0) { //If the number is not divisible by 5 (or 10), the number will be skipped immediately.
			continue;
		} else if (x % 10 === 0) { //If the number is divisible by 10 (which is also divisible by 5), it will be skipped.
			console.log("The number is divisible by 10. Skipping the number.");
			continue;
		} else if (x % 5 === 0) { // If the number is divisible by 5 (which is not divisible by 10), the number is printed.
			console.log(x);
		}
	}
}

printNumber(100);
// printNumber(99);